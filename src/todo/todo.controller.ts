import { Controller, Get, Param, ParseIntPipe, Post, Body, Put, Delete, Next, HttpCode, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { TodoService } from './todo.service';
import { CreateTodoDTO } from './dto/create-todo.dto';
import { UpdateTodoDTO } from './dto/update-todo.dto';
import { Todo } from './todo.entity';
import { ParseBooleanPipe } from 'pipes/parse-boolean/parse-boolean.pipe';

@Controller()
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  index(): Promise<Todo[]> {
    return this.todoService.all();
  }

  @Get('search')
  async search(
    @Query('q') text: string,
    @Query('c', ParseBooleanPipe) completed: boolean | undefined
  ): Promise<Todo> {
    return this.todoService.search(text, completed);
  }

  @Get('search/all')
  async searchAllByText(
    @Query('q') text: string,
    @Query('c', ParseBooleanPipe) completed: boolean
  ): Promise<Todo[]> {
    return this.todoService.searchAll(text, completed);
  }

  @Get(':id')
  async get(@Param('id', ParseIntPipe) id): Promise<Todo> {
    return this.todoService.find(id);
  }

  @Post()
  @UsePipes(new ValidationPipe())
  async store(@Body() data: CreateTodoDTO): Promise<Todo> {
    return this.todoService.create(data);
  }

  @Put(':id')
  async update(
    @Param('id', ParseIntPipe) id,
    @Body() data: UpdateTodoDTO
  ): Promise<Todo> {
    return this.todoService.update(id, data);
  }

  @Delete(':id')
  async delete(@Param('id', ParseIntPipe) id): Promise<void> {
    await this.todoService.delete(id);
  }
}
