import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Todo } from './todo.entity';
import { Repository } from 'typeorm';
import { CreateTodoDTO } from './dto/create-todo.dto';
import { UpdateTodoDTO } from './dto/update-todo.dto';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo)
    private readonly todoRepository: Repository<Todo>
  ) {}

  checkExists(todo: Todo | Todo[]): void {
    if (!todo) {
      throw new NotFoundException();
    }
  }

  async all(): Promise<Todo[]> {
    return this.todoRepository.find();
  }

  async find(id: number): Promise<Todo> {
    const todo = await this.todoRepository.findOne(id);
    this.checkExists(todo);
    return todo;
  }

  async search(text: string, completed: boolean | undefined): Promise<Todo> {
    const todo = await this.searchByTextAndCompleted(text, completed, 'getOne');
    this.checkExists(todo);
    return todo;
  }

  async searchByTextAndCompleted(text: string, completed: boolean | undefined, fetchingMethod: string) {
    if (text && completed === undefined) {
      return this.todoRepository
        .createQueryBuilder('todo')
        .where('todo.text LIKE :text', { text: `%${text}%` })
        [fetchingMethod]();
    }
    if (!text && completed !== undefined) {
      return this.todoRepository
        .createQueryBuilder('todo')
        .where('todo.completed = :condition', { condition: completed })
        [fetchingMethod]();
    }
    if (text && completed !== undefined) {
      return this.todoRepository
        .createQueryBuilder('todo')
        .where('todo.text LIKE :text', { text: `%${text}%` })
        .andWhere('todo.completed = :condition', { condition: completed })
        [fetchingMethod]();
    }
  }

  async searchAll(text: string, completed: boolean | undefined): Promise<Todo[]> {
    const todos = await this.searchByTextAndCompleted(text, completed, 'getMany');
    return todos;
  }

  async create(data: CreateTodoDTO): Promise<Todo> {
    const todo = await this.todoRepository.create(data);
    return this.todoRepository.save(todo);
  }

  async update(id: number, data: UpdateTodoDTO): Promise<Todo> {
    const todo = await this.todoRepository.findOne(id);
    this.checkExists(todo);
    await this.todoRepository.merge(todo, data);
    return this.todoRepository.save(todo);
  }

  async delete(id: number): Promise<void> {
    const todo = await this.todoRepository.findOne(id);
    this.checkExists(todo);
    await this.todoRepository.delete(todo);
  }
}
