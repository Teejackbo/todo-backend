import { IsString, IsInt, IsBoolean, IsNotEmpty } from 'class-validator';

export class CreateTodoDTO {
  @IsString()
  @IsNotEmpty()
  readonly text: string;

  @IsBoolean()
  readonly completed: boolean;
}
