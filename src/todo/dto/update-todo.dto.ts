import { IsString, IsNotEmpty, IsBoolean } from 'class-validator';

export class UpdateTodoDTO {
  @IsString()
  @IsNotEmpty()
  readonly text: string;

  @IsBoolean()
  readonly completed: boolean;
}
