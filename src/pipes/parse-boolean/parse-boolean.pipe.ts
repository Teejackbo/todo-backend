import { ArgumentMetadata, PipeTransform, Injectable } from '@nestjs/common';

@Injectable()
export class ParseBooleanPipe implements PipeTransform {
  async transform(value: string, metadata: ArgumentMetadata) {
    if (value === 'false') return false;
    if (value === undefined) return value;
    return !!value;
  }
}
